#!/usr/bin/env python

import os
import unittest
import numpy as np
import datetime
import pandas as pd

import matplotlib.pyplot as plt
from mpl_toolkits import basemap

from faodata import faodownload, faomap

class FAOMapTestCase(unittest.TestCase):

    def setUp(self):
        source_file = os.path.abspath(__file__)
        self.FOUT = os.path.dirname(source_file)

    def test_get_country_data(self):

        df = faomap.get_countries_shapefiledata()
        self.assertTrue(df.shape == (177, 63))

    def test_map(self):

        # Get data
        database = 'faostat'
        dataset = 'live-prod'
        field = 'm5111'
        year = 2013
        df = faodownload.get_data(database, dataset, field, year=year)

        item = 'Cattle'
        
        idx = df['Item'] == item
        df = df.loc[idx, ['country', 'value']]
        df = df.rename(columns = { \
            'value': 'data', \
            'country': 'iso3' \
        })

        # Draw plot
        plt.close('all')
        fig, ax = plt.subplots()
        map = basemap.Basemap(ax=ax)

        map.drawcoastlines(color='grey')
        map.drawcountries(color='grey')

        cat = [np.percentile(df['data'], pp) 
                for pp in range(10, 100, 10)]

        faomap.plot(map, df, cat, \
                country_field = 'iso3', \
                value_field = 'data', \
                ndigits=0)

        ax.axis('off')
        map.ax.legend(loc=3)
        ax.set_title('%s population, %d' % (item, year),
                fontsize=15)

        faomap.mapfooter(fig, database, dataset, field)

        fig.set_size_inches((16, 8))
        fig.tight_layout()
        fp = '%s/faomap_test.png' % self.FOUT
        fig.savefig(fp)
 

if __name__ == "__main__":
    unittest.main()
